<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="/css/basic.css">
    <script type="text/javascript" src="/js/hello.js"></script>
</head>
<body>
<h3>Hello <b><c:url value="${name}" /></b></h3>
<br/><br/>
<div id="somecolor">some css call</div>
<br/><br/>
<script>
    document.write("<b>some javascript call : </b>" + say_something());
</script>
</body>
</html>