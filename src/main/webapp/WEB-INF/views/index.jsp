<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
</head>
<body>
<b>Index</b>
<br/><br/>
<a href="http://localhost:8088/api/mvc/hello/folks" target="_blank">basic jsp (hello page)</a><br/>
<a href="http://localhost:8088/api/mvc/message" target="_blank">basic jsp (message page)</a><br/>
<a href="http://localhost:8088/api/export/pdf" target="_blank">get a pdf export (standard way)</a><br/>
<a href="http://localhost:8088/api/rest/foo" target="_blank">rest call - foo</a><br/>
<a href="http://localhost:8088/api/rest/time" target="_blank">rest call - time</a><br/>
</body>
</html>
