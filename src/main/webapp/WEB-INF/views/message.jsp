<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
</head>
<body>
Default message : <b><c:url value="${messageDefault}" /></b>
<br/><br/>
French message : <b><c:url value="${messageFrench}" /></b>
<br/><br/>
</body>
</html>