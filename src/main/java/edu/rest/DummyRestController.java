package edu.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
public class DummyRestController {

    @GetMapping("/api/rest/time")
    public @ResponseBody Map<String, Object> getTimeWithRest() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Map<String, Object> map = new HashMap<>();
        map.put("time", sdf.format(new Date()) );
        return map;
    }

    @GetMapping("/api/rest/foo")
    public @ResponseBody Map<String, Object> foo() {
        Map<String, Object> map = new HashMap<>();
        map.put("foo", "bar");
        return map;
    }
}
