package edu.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ResourceBundle;

@Controller
@RequestMapping("/api/mvc")
public class BasicMvcController {

    @GetMapping("/hello/{name}")
    public ModelAndView hello( @PathVariable String name) {
        ModelAndView modelAndView = new ModelAndView("hello");
        modelAndView.addObject("name", name);
        return modelAndView;
    }

    @GetMapping("/message")
    public ModelAndView showMessages() {
        ResourceBundle bundleDefault = ResourceBundle.getBundle("messages");
        ResourceBundle bundleFr = ResourceBundle.getBundle("messages-fr");
        ModelAndView modelAndView = new ModelAndView("message");
        modelAndView.addObject("messageDefault", bundleDefault.getString("helloworld"));
        modelAndView.addObject("messageFrench", bundleFr.getString("helloworld"));
        return modelAndView;
    }

}
