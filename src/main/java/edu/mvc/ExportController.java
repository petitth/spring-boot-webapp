package edu.mvc;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Controller
@RequestMapping("/api/export")
public class ExportController {

    @GetMapping("/pdf")
    public void pdfExport(HttpServletResponse response) throws IOException {
        // this is not a real export since we are using an existing document from ressources
        byte[] fileContent = Files.readAllBytes(Paths.get("./src/main/resources/docs/foobar.pdf"));

        // any export would end up filling the response object
        response.resetBuffer();
        response.setStatus(HttpStatus.OK.value());
        response.setContentType("application/pdf");
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=something.pdf";
        response.setHeader(headerKey, headerValue);
        response.getOutputStream().write(fileContent);
        response.flushBuffer();
    }
}
