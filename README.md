# Testing a web application with Spring Boot

This is a Spring Boot web application with minimal code.

- to launch project : **mvn clean spring-boot:run**
- alternate launch : **mvn clean package && java -jar target/application.jar**
- to test it : open your web browser and call : **http://localhost:8088/**  
remark : feel free to redefine ```server.port``` property in application.yml
